from tkinter import *
root = Tk()
lst = [('ID', 'FULNAME', 'GENDER', 'JOB TITLE', 'STATUS' ),
        (121, 'SEM SOTHEA' , 'MALE', 'STUDENT', 'FREE'  ),
        (421, 'HENG PICHPUNLEU' , 'MALE', 'GRAPHIC DESIGN', 'BUSY'  ),
        (451, 'THY SOPHEAK' , 'MALE', 'STUDENT', 'FREE'  ),
        (531, 'CHEA SANN' , 'MALE', 'STUDENT', 'FREE'  ),
        (551, 'SEN DYRECKHY' , 'MALE', 'MANAGER', 'FREE'),
]
total_rows = len(lst)
total_columns = len(lst[1])
for i in range(total_rows):
    for j in range (total_columns):
        e = Entry(
            root,
            width = 20,
            fg = "black",
            font = ('Arial', 16,) 
            
        )
        e.grid(row = i, column = j)
        e.insert(0, lst[i][j])
root.mainloop()