from tkinter import *
from tkinter import messagebox

def OK(): 
    username = e1.get()
    password = e2.get()

    if(username == "" and password == ""):
        messagebox.showinfo("", "Blank Not Allowed")

    elif(username == "Admin" and password == "Admin"): 
        messagebox.showinfo("", "Log in Success")

    else: 
        messagebox.showinfo("", "Incorrect Username or Password")
    

root =Tk()
root.title("Log In")
root.geometry("300x200")
global e1
global e2 

Lbl = Label (root, text="Verify Account",fg="black",font="Arial" ).place(x=100, y=10)
Label (root, text="Username").place(x=10, y=40)
Label (root, text="Password").place(x=10, y=75)

e1 = Entry(root)
e1.place(x=140, y=40)

e2 =Entry(root)
e2.place(x=140 ,y=75)
e2.config(show="*")

Button(root, text="Cancel" ,command=root.destroy, height = 2, width =10).place(x=30, y=110)
Button(root, text="Verify", command=OK ,height = 2, width =10).place(x=150, y=110)

root.mainloop()